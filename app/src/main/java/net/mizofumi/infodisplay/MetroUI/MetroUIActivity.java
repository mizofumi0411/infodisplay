package net.mizofumi.infodisplay.MetroUI;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.mizofumi.infodisplay.DigitalLED.DigitalLEDView;
import net.mizofumi.infodisplay.R;
import net.mizofumi.infodisplay.StartupActivity;
import net.mizofumi.infodisplay.util.SystemUiHider;

import org.w3c.dom.Text;

/**
 * Created by mizofumi0411 on 14/10/06.
 */
public class MetroUIActivity extends Activity{
    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final boolean TOGGLE_ON_CLICK = true;
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
    private SystemUiHider mSystemUiHider;

    private MetroUIView metroUIView;
    private ImageView todayWeatherIcon;
    private TextView todayWeather;
    private ImageView tomorrowWeatherIcon;
    private TextView tomorrowWeather;
    private TextView date;
    private TextView train;
    private TextView todo;
    private TextView other;
    private TextView lastupdate;
    private TextView news;
    private TextView newstitle;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metroui);
        StartupActivity.pleasewait.setVisibility(View.GONE);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);
        getActionBar().hide();

        todayWeatherIcon = ((ImageView)findViewById(R.id.todayWeatherIcon));
        todayWeather = ((TextView)findViewById(R.id.todayWeather));
        tomorrowWeatherIcon = ((ImageView)findViewById(R.id.tomorrowWeatherIcon));
        tomorrowWeather = ((TextView)findViewById(R.id.tomorrowWeather));
        date = ((TextView)findViewById(R.id.date));
        train = ((TextView)findViewById(R.id.train));
        todo = ((TextView)findViewById(R.id.todo));
        other = ((TextView)findViewById(R.id.other));
        lastupdate = ((TextView)findViewById(R.id.lastupdate));
        news = ((TextView)findViewById(R.id.news));
        newstitle = ((TextView)findViewById(R.id.newstitle));

        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(metroUIView == null){
            metroUIView = new MetroUIView(todayWeatherIcon,todayWeather,tomorrowWeatherIcon,tomorrowWeather,date,train,todo,other,lastupdate,news,newstitle);
            metroUIView.looper();
        }else {
            metroUIView.looper();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
