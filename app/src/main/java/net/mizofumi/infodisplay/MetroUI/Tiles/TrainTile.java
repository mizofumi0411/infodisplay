package net.mizofumi.infodisplay.MetroUI.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import net.mizofumi.infodisplay.MetroUI.MetroUIView;

/**
 * Created by mizofumi0411 on 14/10/06.
 */
public class TrainTile {
    private TextView text;

    public TrainTile(TextView text) {
        this.text = text;
    }


    public void looper(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if(MetroUIView.trainUtil != null) {
                                text.setText(MetroUIView.trainUtil.getTelop());
                            }
                        }
                    });
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
