package net.mizofumi.infodisplay.MetroUI.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import net.mizofumi.infodisplay.MetroUI.MetroUIView;
import net.mizofumi.infodisplay.WeatherUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mizofumi0411 on 14/10/06.
 */
public class TodayWeatherTile {

    private ImageView icon;
    private TextView text;

    int page = 0;

    public TodayWeatherTile(ImageView icon, TextView text) {
        this.icon = icon;
        this.text = text;
    }

    public void looper(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if(MetroUIView.weatherUtil != null) {
                                icon.setImageResource(MetroUIView.weatherUtil.getIcon(MetroUIView.weatherUtil.getTodaytelop()));

                                switch (page) {
                                    case 0:
                                        text.setText(MetroUIView.weatherUtil.getTodaytelop());
                                        break;
                                    case 1:
                                        text.setText("最高:" + MetroUIView.weatherUtil.getTodaymaxtemp());
                                        break;
                                    case 2:
                                        text.setText("最低:" + MetroUIView.weatherUtil.getTodaymintemp());
                                        page = -1;
                                        break;
                                }
                                page += 1;
                            }
                        }
                    });
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
