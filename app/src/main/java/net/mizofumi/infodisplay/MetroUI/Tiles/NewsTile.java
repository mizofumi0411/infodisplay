package net.mizofumi.infodisplay.MetroUI.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

import net.mizofumi.infodisplay.MetroUI.MetroUIView;

import java.util.ArrayList;

/**
 * Created by mizofumi0411 on 14/10/06.
 */
public class NewsTile {
    private TextView text;

    private int page = 0;

    public NewsTile(TextView text) {
        this.text = text;
    }

    public void looper(){

        new Thread(new Runnable() {
            @Override
            public void run() {

                while (true){
                    if(MetroUIView.newsUtil != null){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<String> newses;
                                newses = new ArrayList<String>();
                                for (String title:MetroUIView.newsUtil.getTopics()){
                                    newses.add("[トピックス]\t"+title);
                                }
                                for (String title:MetroUIView.newsUtil.getDomestices()){
                                    newses.add("[国内]\t"+title);
                                }
                                for (String title:MetroUIView.newsUtil.getComputers()){
                                    newses.add("[コンピュータ]\t"+title);
                                }
                                Log.d("newsPage", String.valueOf(page));
                                if(MetroUIView.newsUtil != null){
                                    text.setText(newses.get(page));
                                }
                                if(page == newses.size()-1) page = -1;
                                page+=1;
                            }
                        });
                    }

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
