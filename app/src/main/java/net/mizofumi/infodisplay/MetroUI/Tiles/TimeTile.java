package net.mizofumi.infodisplay.MetroUI.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mizofumi0411 on 14/10/06.
 */
public class TimeTile {
    private TextView date;
    public static boolean runnable = false;

    public TimeTile(TextView date) {
        this.date = date;
        this.runnable = true;
    }

    public void looper(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Date d = new Date();
                            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd[E]\nkk:mm.ss");
                            date.setText(sdf.format(d));
                            Log.d("date",sdf.format(d));
                        }
                    });
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
