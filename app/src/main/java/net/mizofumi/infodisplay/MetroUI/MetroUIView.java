package net.mizofumi.infodisplay.MetroUI;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import net.mizofumi.infodisplay.MetroUI.Tiles.NewsTile;
import net.mizofumi.infodisplay.MetroUI.Tiles.TimeTile;
import net.mizofumi.infodisplay.MetroUI.Tiles.TodayWeatherTile;
import net.mizofumi.infodisplay.MetroUI.Tiles.TomorrowWeatherTile;
import net.mizofumi.infodisplay.MetroUI.Tiles.TrainTile;
import net.mizofumi.infodisplay.NewsUtil;
import net.mizofumi.infodisplay.R;
import net.mizofumi.infodisplay.TrainUtil;
import net.mizofumi.infodisplay.WeatherUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mizofumi0411 on 14/10/06.
 */
public class MetroUIView {
    private ImageView todayWeatherIcon;
    private TextView todayWeather;
    private ImageView tomorrowWeatherIcon;
    private TextView tomorrowWeather;
    private TextView date;
    private TextView train;
    private TextView todo;
    private TextView other;
    private TextView lastupdate;
    private TextView news;
    private TextView newstitle;

    private TimeTile timeTile;
    private TodayWeatherTile todayWeatherTile;
    private TomorrowWeatherTile tomorrowWeatherTile;
    private TrainTile trainTile;
    private NewsTile newsTiles;

    public static WeatherUtil weatherUtil; //天気情報の管理クラス
    public static TrainUtil trainUtil;
    public static NewsUtil newsUtil;

    Date _lastupdate;
    int reloadtime = 1;
    String city = "120010";

    public MetroUIView(ImageView todayWeatherIcon, TextView todayWeather, ImageView tomorrowWeatherIcon, TextView tomorrowWeather, TextView date, TextView train, TextView todo, TextView other, TextView lastupdate, TextView news, TextView newstitle) {
        this.todayWeatherIcon = todayWeatherIcon;
        this.todayWeather = todayWeather;
        this.tomorrowWeatherIcon = tomorrowWeatherIcon;
        this.tomorrowWeather = tomorrowWeather;
        this.date = date;
        this.train = train;
        this.todo = todo;
        this.other = other;
        this.lastupdate = lastupdate;
        this.news = news;
        this.newstitle = newstitle;
    }

    public void looper(){
        timeTile = new TimeTile(date);
        timeTile.looper();
        todayWeatherTile = new TodayWeatherTile(todayWeatherIcon,todayWeather);
        todayWeatherTile.looper();
        tomorrowWeatherTile = new TomorrowWeatherTile(tomorrowWeatherIcon,tomorrowWeather);
        tomorrowWeatherTile.looper();
        trainTile = new TrainTile(train);
        trainTile.looper();
        newsTiles = new NewsTile(news);
        newsTiles.looper();

        //情報取得ルーパー
        new Thread(new Runnable() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        todayWeatherIcon.setImageResource(R.drawable.reload);
                        tomorrowWeatherIcon.setImageResource(R.drawable.reload);
                        lastupdate.setText("未更新");
                    }
                });
                while (true){
                    reload();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    //情報更新メソッド
    private void reload(){
        Date nowdate = new Date();
        if(_lastupdate == null){
            weatherUtil = new WeatherUtil(city);
            trainUtil = new TrainUtil();
            try {
                newsUtil = new NewsUtil();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(nowdate);
            cal.add(Calendar.MINUTE,reloadtime);
            _lastupdate = cal.getTime();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd kk:mm.ss");
                    todayWeatherIcon.setImageResource(R.drawable.reload);
                    todayWeather.setText("更新中..");
                    tomorrowWeatherIcon.setImageResource(R.drawable.reload);
                    tomorrowWeather.setText("更新中..");
                    train.setText("更新中..");
                    news.setText("更新中..");
                    lastupdate.setText("NextUpdate:"+sdf.format(_lastupdate));
                }
            });
        }

        if(nowdate.after(_lastupdate)) {
            weatherUtil = new WeatherUtil(city);
            trainUtil = new TrainUtil();
            try {
                newsUtil = new NewsUtil();
            } catch (Exception e) {
                e.printStackTrace();
            }
            nowdate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(nowdate);
            cal.add(Calendar.MINUTE,reloadtime);
            _lastupdate = cal.getTime();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd kk:mm.ss");
                    todayWeatherIcon.setImageResource(R.drawable.reload);
                    todayWeather.setText("更新中..");
                    tomorrowWeatherIcon.setImageResource(R.drawable.reload);
                    todayWeather.setText("更新中..");
                    train.setText("更新中..");
                    news.setText("更新中..");
                    lastupdate.setText("NextUpdate:"+sdf.format(_lastupdate));
                }
            });
        }
        Log.d("updateTime",_lastupdate.toString());
    }

}
