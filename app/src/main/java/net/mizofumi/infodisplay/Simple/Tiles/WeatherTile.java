package net.mizofumi.infodisplay.Simple.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import android.widget.TextView;

import net.mizofumi.infodisplay.WeatherUtil;

/**
 * Created by mizofumi0411 on 2014/10/14.
 */
public class WeatherTile {

    int page = 0;

    WeatherUtil weatherUtil;
    TextView weatherDate;
    TextView weatherTelop;
    TextView weatherMax;
    TextView weatherMin;
    ImageView weatherIcon;

    public WeatherTile(WeatherUtil weatherUtil,TextView weatherDate, TextView weatherTelop, TextView weatherMax, TextView weatherMin, ImageView weatherIcon) {
        this.weatherUtil = weatherUtil;
        this.weatherDate = weatherDate;
        this.weatherTelop = weatherTelop;
        this.weatherMax = weatherMax;
        this.weatherMin = weatherMin;
        this.weatherIcon = weatherIcon;
    }

    public void looper(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            switch (page){
                                case 0:
                                    weatherDate.setText("Today");
                                    weatherTelop.setText(weatherUtil.getTodaytelop());
                                    weatherMax.setText(weatherUtil.getTodaymaxtemp());
                                    weatherMin.setText(weatherUtil.getTodaymintemp());
                                    weatherIcon.setImageResource(weatherUtil.getNewIcon(weatherUtil.getTodaytelop()));
                                    page = 1;
                                    break;
                                case 1:
                                    weatherDate.setText("Tomorrow");
                                    weatherTelop.setText(weatherUtil.getTomorrowtelop());
                                    weatherMax.setText(weatherUtil.getTomorrowmaxtemp());
                                    weatherMin.setText(weatherUtil.getTomorrowmintemp());
                                    weatherIcon.setImageResource(weatherUtil.getNewIcon(weatherUtil.getTomorrowtelop()));
                                    page = 0;
                                    break;
                            }
                        }
                    });

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
