package net.mizofumi.infodisplay.Simple;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.mizofumi.infodisplay.R;
import net.mizofumi.infodisplay.util.SystemUiHider;

import org.json.JSONException;

import java.io.IOException;

public class SimpleActivity extends Activity {
    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final boolean TOGGLE_ON_CLICK = true;
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
    private SystemUiHider mSystemUiHider;
    SimpleView simpleview;

    RelativeLayout reload;
    ImageView reloadIcon;

    TextView dateTelop;
    TextView weatherDate;
    TextView weatherTelop;
    TextView weatherMax;
    TextView weatherMin;
    ImageView weatherIcon;

    TextView trainTelop;
    TextView otherTelop;
    TextView newsTelop;
    TextView todoTelop;
    ImageView charaImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple);
        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        getActionBar().hide();

        reload = (RelativeLayout)findViewById(R.id.reload);
        reloadIcon = (ImageView)findViewById(R.id.reloadIcon);

        dateTelop = (TextView)findViewById(R.id.dateTelop);
        weatherDate = (TextView)findViewById(R.id.weatherDate);
        weatherTelop = (TextView)findViewById(R.id.weatherTelop);
        weatherMax = (TextView)findViewById(R.id.weatherMax);
        weatherMin = (TextView)findViewById(R.id.weatherMin);
        weatherIcon = (ImageView)findViewById(R.id.weatherIcon);

        trainTelop = (TextView)findViewById(R.id.trainTelop);
        otherTelop = (TextView)findViewById(R.id.otherTelop);
        newsTelop = (TextView)findViewById(R.id.newsTelop);
        todoTelop = (TextView)findViewById(R.id.todoTelop);
        charaImage = (ImageView)findViewById(R.id.charaImage);



        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        simpleview = new SimpleView(reload,reloadIcon,dateTelop,weatherDate,weatherTelop,weatherMax,weatherMin,weatherIcon,trainTelop,otherTelop,newsTelop,todoTelop,charaImage);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    simpleview.onUpdate();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(SimpleActivity.this,"初期更新に失敗しました",Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SimpleActivity.this,"初期更新に失敗しました",Toast.LENGTH_SHORT).show();
                }
                simpleview.looper();
            }
        }).start();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

}
