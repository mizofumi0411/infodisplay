package net.mizofumi.infodisplay.Simple.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import net.mizofumi.infodisplay.MetroUI.MetroUIView;
import net.mizofumi.infodisplay.TrainUtil;

/**
 * Created by mizofumi0411 on 2014/10/14.
 */
public class TrainTile {

    private TextView text;
    private TrainUtil trainUtil;

    public TrainTile(TrainUtil trainUtil,TextView text) {
        this.text = text;
        this.trainUtil = trainUtil;
    }

    public void looper(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if(trainUtil != null) {
                                text.setText(trainUtil.getTelop());
                            }
                        }
                    });
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

}
