package net.mizofumi.infodisplay.Simple;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.mizofumi.infodisplay.Simple.Tiles.TrainTile;
import net.mizofumi.infodisplay.NewsUtil;
import net.mizofumi.infodisplay.R;
import net.mizofumi.infodisplay.Simple.Tiles.CharaTile;
import net.mizofumi.infodisplay.Simple.Tiles.NewsTile;
import net.mizofumi.infodisplay.Simple.Tiles.TimeTile;
import net.mizofumi.infodisplay.Simple.Tiles.WeatherTile;
import net.mizofumi.infodisplay.TrainUtil;
import net.mizofumi.infodisplay.WeatherUtil;

import org.json.JSONException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mizofumi0411 on 2014/10/14.
 */
public class SimpleView {
    RelativeLayout reload;
    ImageView reloadIcon;

    TextView dateTelop;
    TextView weatherDate;
    TextView weatherTelop;
    TextView weatherMax;
    TextView weatherMin;
    ImageView weatherIcon;

    TextView trainTelop;
    TextView otherTelop;
    TextView newsTelop;
    TextView todoTelop;
    ImageView charaImage;

    WeatherUtil weatherUtil; //天気情報の管理クラス
    TrainUtil trainUtil; //鉄道情報の管理クラス
    NewsUtil newsUtil;

    TimeTile timeTile;
    WeatherTile weatherTile;
    TrainTile trainTile;
    NewsTile newsTile;
    CharaTile charaTile;

    Date _lastupdate;
    int reloadtime = 1;


    public SimpleView(RelativeLayout relaod,ImageView reloadIcon,TextView dateTelop, TextView weatherDate, TextView weatherTelop, TextView weatherMax, TextView weatherMin, ImageView weatherIcon, TextView trainTelop, TextView otherTelop, TextView newsTelop, TextView todoTelop, ImageView charaImage) {
        this.reload = relaod;
        this.reloadIcon = reloadIcon;
        this.dateTelop = dateTelop;
        this.weatherDate = weatherDate;
        this.weatherTelop = weatherTelop;
        this.weatherMax = weatherMax;
        this.weatherMin = weatherMin;
        this.weatherIcon = weatherIcon;
        this.trainTelop = trainTelop;
        this.otherTelop = otherTelop;
        this.newsTelop = newsTelop;
        this.todoTelop = todoTelop;
        this.charaImage = charaImage;
    }


    public void looper(){

        timeTile = new TimeTile(dateTelop);
        timeTile.looper();
        weatherTile = new WeatherTile(weatherUtil,weatherDate,weatherTelop,weatherMax,weatherMin,weatherIcon);
        weatherTile.looper();
        trainTile = new TrainTile(trainUtil,trainTelop);
        trainTile.looper();
        newsTile = new NewsTile(newsUtil,newsTelop);
        newsTile.looper();
        charaTile = new CharaTile(charaImage);
        charaTile.looper();


        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    try {
                        onUpdate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                reloadIcon.setImageResource(R.drawable.error2);
                                reload.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }


    public void onUpdate() throws IOException, JSONException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                reloadIcon.setImageResource(R.drawable.refresh);
                reload.setVisibility(View.VISIBLE);
            }
        });
        Date nowdate = new Date();
        weatherUtil = new WeatherUtil("120010");
        trainUtil = new TrainUtil();
        newsUtil = new NewsUtil();
        Calendar cal = Calendar.getInstance();
        cal.setTime(nowdate);
        cal.add(Calendar.MINUTE,reloadtime);
        _lastupdate = cal.getTime();
        /*
        if(_lastupdate == null){
            weatherUtil = new WeatherUtil("120010");
            trainUtil = new TrainUtil();
            newsUtil = new NewsUtil();
            Calendar cal = Calendar.getInstance();
            cal.setTime(nowdate);
            cal.add(Calendar.MINUTE,reloadtime);
            _lastupdate = cal.getTime();
        }

        if(nowdate.after(_lastupdate)) {
            weatherUtil = new WeatherUtil("120010");
            trainUtil = new TrainUtil();
            newsUtil = new NewsUtil();
            nowdate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(nowdate);
            cal.add(Calendar.MINUTE,reloadtime);
            _lastupdate = cal.getTime();
        }
        */
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                reloadIcon.setImageResource(R.drawable.refresh);
                reload.setVisibility(View.GONE);
            }
        });

        Log.d("updateTime", _lastupdate.toString());
    }
}
