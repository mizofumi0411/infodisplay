package net.mizofumi.infodisplay.Simple.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

import net.mizofumi.infodisplay.MetroUI.MetroUIView;
import net.mizofumi.infodisplay.NewsUtil;

import java.util.ArrayList;

/**
 * Created by mizofumi0411 on 2014/10/14.
 */
public class NewsTile {
    private NewsUtil newsUtil;
    private TextView text;

    private int page = 0;

    public NewsTile(NewsUtil newsUtil, TextView text) {
        this.newsUtil = newsUtil;
        this.text = text;
    }

    public void looper(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            text.setText("");
                            if(newsUtil != null){
                                switch (page){
                                    case 0:
                                        for (String title:newsUtil.getTopics()){
                                            text.setText(text.getText()+"[Topics]\t"+title+"\n");
                                        }
                                        page = 1;
                                        break;
                                    case 1:
                                        for (String title:newsUtil.getDomestices()){
                                            text.setText(text.getText()+"[国内]\t"+title+"\n");
                                        }
                                        page = 2;
                                        break;
                                    case 2:
                                        for (String title:newsUtil.getComputers()){
                                            text.setText(text.getText()+"[ｺﾝﾋﾟｭｰﾀ]\t"+title+"\n");
                                        }
                                        page = 0;
                                        break;

                                }
                            }
                        }
                    });


                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
