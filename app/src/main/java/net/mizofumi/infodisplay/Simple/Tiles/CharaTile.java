package net.mizofumi.infodisplay.Simple.Tiles;

import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;

import net.mizofumi.infodisplay.R;

import java.util.Random;

/**
 * Created by mizofumi0411 on 2014/10/14.
 */
public class CharaTile {
    int value = 0;
    ImageView image;

    public CharaTile(ImageView image) {
        this.image = image;
    }

    public void looper(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Random rnd = new Random();
                            int ran;
                            switch (value){
                                case 0:
                                    image.setImageResource(R.drawable.ika1);
                                    ran = rnd.nextInt(10);
                                    if(ran == 0) value = 1;
                                    break;
                                case 1:
                                    image.setImageResource(R.drawable.ika2);
                                    ran = rnd.nextInt(10);
                                    if(ran > 2) value = 2;
                                    break;
                                case 2:
                                    image.setImageResource(R.drawable.ika3);
                                    value = 0;
                                    break;
                            }
                        }
                    });

                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();
    }
}
