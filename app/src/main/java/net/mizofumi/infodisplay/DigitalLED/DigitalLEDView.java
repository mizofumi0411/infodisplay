package net.mizofumi.infodisplay.DigitalLED;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import net.mizofumi.infodisplay.R;
import net.mizofumi.infodisplay.TrainUtil;
import net.mizofumi.infodisplay.WeatherUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mizofumi0411 on 2014/09/27.
 */
public class DigitalLEDView {

    ImageView icon;
    TextView text;

    /*
    天気情報の場所情報
    コード番号は下記URLを参照
    http://weather.livedoor.com/forecast/rss/primary_area.xml
     */
    String city = "120010";

    int page = 0; //最初のページ番号
    int resetpage = 8; //最後のページ番号
    int waittime = 1500; //切り替えるまでの時間
    int reloadtime = 1; //情報の更新間隔(分)
    Date lastupdate;

    WeatherUtil weatherUtil; //天気情報の管理クラス
    TrainUtil trainUtil; //鉄道情報の管理クラス

    public DigitalLEDView(ImageView icon, TextView text) {
        this.icon = icon;
        this.text = text;
    }

    public void looper(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        icon.setImageResource(R.drawable.reload);
                        text.setText("更新中..");
                        text.setTextSize(100.0f);
                    }
                });
                weatherUtil = new WeatherUtil(city);
                trainUtil = new TrainUtil();

                while (true){
                    reload();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Date date = new Date();

                            switch (page){
                                case 0:
                                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd[E]\nkk:mm.ss");
                                    icon.setImageResource(R.drawable.clock);
                                    text.setText(sdf.format(date));
                                    text.setTextSize(76.0f);

                                    SimpleDateFormat youbi = new SimpleDateFormat("E");
                                    //可燃物:火木土
                                    if(!(youbi.format(date).equals("火")||youbi.format(date).equals("木")||youbi.format(date).equals("土"))){
                                        page += 1;
                                    }
                                    break;
                                case 1:
                                    youbi = new SimpleDateFormat("E");
                                    //可燃物:火木土
                                    if(youbi.format(date).equals("火")||youbi.format(date).equals("木")||youbi.format(date).equals("土")){
                                        icon.setImageResource(R.drawable.trash);
                                        text.setText("可燃物の日");
                                        text.setTextSize(90.0f);
                                    }else {
                                        break;
                                    }
                                    break;
                                case 2:
                                    icon.setImageResource(weatherUtil.getIcon(weatherUtil.getTodaytelop()));
                                    text.setText(weatherUtil.getLocation()+":"+weatherUtil.getTodaydatetelop());
                                    text.setTextSize(75.0f);
                                    break;
                                case 3:
                                    icon.setImageResource(weatherUtil.getIcon(weatherUtil.getTodaytelop()));
                                    text.setText(weatherUtil.getTodaytelop());
                                    text.setTextSize(80.0f);
                                    break;
                                case 4:
                                    text.setText("最高:"+weatherUtil.getTodaymaxtemp()+"\n最低:"+weatherUtil.getTodaymintemp());
                                    text.setTextSize(70.0f);
                                    break;

                                case 5:
                                    icon.setImageResource(weatherUtil.getIcon(weatherUtil.getTomorrowtelop()));
                                    text.setText(weatherUtil.getLocation()+":"+weatherUtil.getTomorrowdatetelop());
                                    text.setTextSize(75.0f);
                                    break;
                                case 6:
                                    icon.setImageResource(weatherUtil.getIcon(weatherUtil.getTomorrowtelop()));
                                    text.setText(weatherUtil.getTomorrowtelop());
                                    text.setTextSize(80.0f);
                                    break;
                                case 7:
                                    text.setText("最高:"+weatherUtil.getTomorrowmaxtemp()+"\n最低:"+weatherUtil.getTomorrowmintemp());
                                    text.setTextSize(70.0f);
                                    break;
                                case 8:
                                    text.setText("鉄道情報\n"+trainUtil.getTelop());
                                    text.setTextSize(50.0f);
                                    icon.setImageResource(R.drawable.train);
                            }
                        }
                    });

                    try {
                        Thread.sleep(waittime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(page == resetpage)page=-1;
                    page += 1;
                }

            }
        }).start();
    }

    //情報更新メソッド
    private void reload() {
        Date nowdate = new Date();
        if(lastupdate == null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(nowdate);
            cal.add(Calendar.MINUTE,reloadtime);
            lastupdate = cal.getTime();
        }

        Log.d("date1",nowdate.toString());
        Log.d("date2",lastupdate.toString());

        if(nowdate.after(lastupdate)) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    icon.setImageResource(R.drawable.reload);
                    text.setText("更新中..");
                    text.setTextSize(100.0f);
                }
            });
            weatherUtil = new WeatherUtil(city);
            trainUtil = new TrainUtil();
            nowdate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(nowdate);
            cal.add(Calendar.MINUTE,reloadtime);
            lastupdate = cal.getTime();
        }
    }
}
