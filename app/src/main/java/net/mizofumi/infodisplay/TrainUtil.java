package net.mizofumi.infodisplay;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mizofumi0411 on 14/09/30.
 */
public class TrainUtil {
    private String jsonUrl = "http://rti-giken.jp/fhc/api/train_tetsudo/delay.json";
    private String data;
    private boolean none = true;
    private String telop = "";

    public TrainUtil() {
        HttpClient httpClient = new DefaultHttpClient();

        StringBuilder url = new StringBuilder(jsonUrl);
        HttpGet request = new HttpGet(url.toString());
        HttpResponse response;

        try {
            response = httpClient.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                response.getEntity().writeTo(outputStream);
                data = outputStream.toString();
                Log.d("train",data);
                try {
                    JSONArray rootArray = new JSONArray(data);
                    for (int i = 0; i < rootArray.length(); i++) {
                        JSONObject rootArrayJSONObject = rootArray.getJSONObject(i);
                        rootArrayJSONObject.getString("name");
                        if(rootArrayJSONObject.getString("name").indexOf("総武緩行線")==0){
                            telop += rootArrayJSONObject.getString("name")+":情報有"+"\n";
                            none = false;
                            Log.d("train",telop);
                        }
                        if(rootArrayJSONObject.getString("name").indexOf("総武快速線")==0){
                            telop += rootArrayJSONObject.getString("name")+":情報有"+"\n";
                            none = false;
                            Log.d("train",telop);
                        }
                        if(rootArrayJSONObject.getString("name").indexOf("京成")==0){
                            telop += rootArrayJSONObject.getString("name")+":情報有"+"\n";
                            none = false;
                            Log.d("train",telop);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public String getTelop() {
        return telop;
    }
}
