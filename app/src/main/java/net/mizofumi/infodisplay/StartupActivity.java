package net.mizofumi.infodisplay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import net.mizofumi.infodisplay.DigitalLED.DigitalLEDActivity;
import net.mizofumi.infodisplay.MetroUI.MetroUIActivity;
import net.mizofumi.infodisplay.Simple.SimpleActivity;


public class StartupActivity extends Activity {

    public static RelativeLayout pleasewait;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        pleasewait = ((RelativeLayout)findViewById(R.id.pleasewait));

        ((Button)findViewById(R.id.digitalledbutton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartupActivity.this, DigitalLEDActivity.class);
                startActivity(intent);
                //pleasewait.setVisibility(View.VISIBLE);
            }
        });

        ((Button)findViewById(R.id.metrobutton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartupActivity.this, MetroUIActivity.class);
                startActivity(intent);
                //pleasewait.setVisibility(View.VISIBLE);
            }
        });

        ((Button)findViewById(R.id.simplebutton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartupActivity.this, SimpleActivity.class);
                startActivity(intent);
                //pleasewait.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.startup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
