package net.mizofumi.infodisplay;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by mizofumi0411 on 2014/09/27.
 */
public class WeatherUtil {

    private String jsonUrl = "http://weather.livedoor.com/forecast/webservice/json/v1?city=";
    private String data = null;
    private String location;
    private String todaydatetelop;
    private String todaytelop;
    private String todaymaxtemp;
    private String todaymintemp;
    private String tomorrowdatetelop;
    private String tomorrowtelop;
    private String tomorrowmaxtemp;
    private String tomorrowmintemp;


    public WeatherUtil(String city) {
        HttpClient httpClient = new DefaultHttpClient();

        StringBuilder url = new StringBuilder(jsonUrl+city);
        HttpGet request = new HttpGet(url.toString());
        HttpResponse response = null;

        try {
            response = httpClient.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                response.getEntity().writeTo(outputStream);
                data = outputStream.toString();

                //場所情報取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONObject loc = rootObject.getJSONObject("location");
                    location = loc.getString("city");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //日付テロップ取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(0);
                    todaydatetelop = forecast.getString("dateLabel");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //テロップ取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(0);
                    todaytelop = forecast.getString("telop");
                    Log.d("updateWeather",todaytelop);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //最高気温取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(0);
                    JSONObject temperature = forecast.getJSONObject("temperature");
                    JSONObject max = temperature.getJSONObject("max");
                    todaymaxtemp = max.getString("celsius")+"℃";
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //最低気温取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(0);
                    JSONObject temperature = forecast.getJSONObject("temperature");
                    JSONObject min = temperature.getJSONObject("min");
                    todaymintemp = min.getString("celsius")+"℃";
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //日付テロップ取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(1);
                    tomorrowdatetelop = forecast.getString("dateLabel");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //テロップ取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(1);
                    tomorrowtelop = forecast.getString("telop");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //最高気温取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(1);
                    JSONObject temperature = forecast.getJSONObject("temperature");
                    JSONObject max = temperature.getJSONObject("max");
                    tomorrowmaxtemp = max.getString("celsius")+"℃";
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //最低気温取得
                try {
                    JSONObject rootObject = new JSONObject(data);
                    JSONArray forecasts = rootObject.getJSONArray("forecasts");
                    JSONObject forecast = forecasts.getJSONObject(1);
                    JSONObject temperature = forecast.getJSONObject("temperature");
                    JSONObject min = temperature.getJSONObject("min");
                    tomorrowmintemp = min.getString("celsius")+"℃";
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getLocation() {
        return location;
    }

    public String getTodaydatetelop() {
        return todaydatetelop;
    }

    public String getTodaytelop(){
        return todaytelop;
    }

    public String getTodaymaxtemp(){
        if(todaymaxtemp != null && todaymaxtemp.equals("null")) return "--";
        return todaymaxtemp;
    }

    public String getTodaymintemp() {
        if(todaymintemp != null && todaymintemp.equals("null")) return "--";
        return todaymintemp;
    }

    public String getTomorrowdatetelop() {
        return tomorrowdatetelop;
    }

    public String getTomorrowtelop() {
        return tomorrowtelop;
    }

    public String getTomorrowmaxtemp() {
        if(tomorrowmaxtemp != null && tomorrowmaxtemp.equals("null")) return "--";
        return tomorrowmaxtemp;
    }

    public String getTomorrowmintemp() {
        if(tomorrowmintemp != null && tomorrowmintemp.equals("null")) return "--";
        return tomorrowmintemp;
    }

    public int getIcon(String telop){
        if(telop != null){

            //雨と曇りの文言がない晴れの場合は快晴アイコン
            if(telop.indexOf("晴") != -1){
                if((telop.indexOf("雨") == -1)&&(telop.indexOf("曇")==-1)){
                    return R.drawable.sun;
                }
            }
            //晴れと曇りの文言はあるが雨と雷の文言が無ければ太陽と雲のアイコン
            if((telop.indexOf("晴")!=-1) && (telop.indexOf("曇")!=-1)){
                if((telop.indexOf("雨")==-1) && (telop.indexOf("雷")==-1)){
                    return R.drawable.suncloud;
                }
            }

            //雷の文言のない雨の場合は雨アイコン
            if((telop.indexOf("雨")!=-1)&&(telop.indexOf("雷")==-1)){
                return R.drawable.rain;
            }

            //雨と雷の文言があれば雨と雷のアイコン
            if((telop.indexOf("雨")!=-1)&&(telop.indexOf("雷")!=-1)){
                return R.drawable.thunderrain;
            }

            //曇のみの文言の場合は雲のアイコン
            if(telop.indexOf("曇")!=-1){
                return R.drawable.cloud;
            }

            //雷のみの文言の場合は雷アイコン
            if((telop.indexOf("雷")!=-1)){
                return R.drawable.thunder;
            }
        }else {
            //情報がない場合はエラーアイコン
            return R.drawable.error;
        }

        //どのパターンにも当てはまらなかったらエラーアイコン
        return R.drawable.error;
    }

    public int getNewIcon(String telop){
        if(telop != null){

            //雨と曇りの文言がない晴れの場合は快晴アイコン
            if(telop.indexOf("晴") != -1){
                if((telop.indexOf("雨") == -1)&&(telop.indexOf("曇")==-1)){
                    return R.drawable.sunny;
                }
            }
            //晴れと曇りの文言はあるが雨と雷の文言が無ければ太陽と雲のアイコン
            if((telop.indexOf("晴")!=-1) && (telop.indexOf("曇")!=-1)){
                if((telop.indexOf("雨")==-1) && (telop.indexOf("雷")==-1)){
                    return R.drawable.mostlycloudy;
                }
            }

            //雷の文言のない雨の場合は雨アイコン
            if((telop.indexOf("雨")!=-1)&&(telop.indexOf("雷")==-1)){
                return R.drawable.drizzle;
            }

            //雨と雷の文言があれば雨と雷のアイコン
            if((telop.indexOf("雨")!=-1)&&(telop.indexOf("雷")!=-1)){
                return R.drawable.thunderstorms;
            }

            //曇のみの文言の場合は雲のアイコン
            if(telop.indexOf("曇")!=-1){
                return R.drawable.cloudy;
            }

            //雷のみの文言の場合は雷アイコン
            if((telop.indexOf("雷")!=-1)){
                return R.drawable.thunderstorms;
            }
        }else {
            //情報がない場合はエラーアイコン
            return R.drawable.error2;
        }

        //どのパターンにも当てはまらなかったらエラーアイコン
        return R.drawable.error2;
    }


}
