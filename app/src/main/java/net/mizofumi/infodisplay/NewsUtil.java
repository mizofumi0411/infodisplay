package net.mizofumi.infodisplay;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mizofumi0411 on 14/10/06.
 */
public class NewsUtil {
    private String topic = ""; //トピック
    private String domestic = "domestic/"; //国内
    private String computer = "computer/"; //コンピュータ
    private String headUrl = "https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=http://rss.dailynews.yahoo.co.jp/fc/";
    private String footUrl = "rss.xml&num=-1";

    private ArrayList<String> topics = new ArrayList<String>();
    private ArrayList<String> domestices = new ArrayList<String>();
    private ArrayList<String> computers = new ArrayList<String>();

    public NewsUtil() throws IOException, JSONException {
        //トピック取得
        HttpResponse response = getData(topic);
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            response.getEntity().writeTo(outputStream);
            JSONObject rootObject = new JSONObject(outputStream.toString());
            JSONObject responseData = rootObject.getJSONObject("responseData");
            JSONObject feed = responseData.getJSONObject("feed");
            JSONArray entries = feed.getJSONArray("entries");
            for (int i =0;i < entries.length();i++){
                topics.add(entries.getJSONObject(i).getString("title"));
            }
        }

        //国内取得
        response = getData(domestic);
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            response.getEntity().writeTo(outputStream);
            JSONObject rootObject = new JSONObject(outputStream.toString());
            JSONObject responseData = rootObject.getJSONObject("responseData");
            JSONObject feed = responseData.getJSONObject("feed");
            JSONArray entries = feed.getJSONArray("entries");
            for (int i =0;i < entries.length();i++){
                domestices.add(entries.getJSONObject(i).getString("title"));
            }
        }

        //コンピュータ取得
        response = getData(computer);
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            response.getEntity().writeTo(outputStream);
            JSONObject rootObject = new JSONObject(outputStream.toString());
            JSONObject responseData = rootObject.getJSONObject("responseData");
            JSONObject feed = responseData.getJSONObject("feed");
            JSONArray entries = feed.getJSONArray("entries");
            for (int i =0;i < entries.length();i++){
                computers.add(entries.getJSONObject(i).getString("title"));
            }
        }
    }

    private HttpResponse getData(String type){
        HttpClient httpClient = new DefaultHttpClient();

        StringBuilder url = new StringBuilder(headUrl+type+footUrl);
        Log.d("newsHttp",headUrl+type+footUrl);
        HttpGet request = new HttpGet(url.toString());
        try {
            return httpClient.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<String> getTopics() {
        return topics;
    }

    public ArrayList<String> getDomestices() {
        return domestices;
    }

    public ArrayList<String> getComputers() {
        return computers;
    }

}
